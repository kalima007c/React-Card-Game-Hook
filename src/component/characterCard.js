import React, { useState ,useRef , useEffect } from 'react';
import '../App.css';

const CharacterCard = (props) => {
const [active,setActive] = useState(false)
const attemptRef = useRef(props.attempt);

useEffect(()=>{
    if(attemptRef.current !== props.attempt){
        setActive(false)
        attemptRef.current = props.attempt
    }
},[])

const activate = () => { /* set สถานะของ card ที่ถูกเลือก */ 
    if(!active) {
        setActive(true)
        props.activationHandler(props.value)
    }
}

const className = `card ${active ? 'activeCard': ''}`

    return(
        <div className = {className} onClick={activate}>
            { props.value }
        </div>
    )
}

export default CharacterCard