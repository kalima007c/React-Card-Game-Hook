import React , {useState, useEffect}from 'react'
import CharacterCard from './CharacterCard'
import _ from 'lodash';
// เรียกประกาศฟังก์ชันเพื่อเรียกใช้ ไลบรารี lodash เพื่อใช้จัดการอาร์เรย์ได้ง่ายขึ้น จำเป็นต้องติดตั้งผ่าน yarn add lodash ก่อน
const prepareStateFromWord = (given_word) => { 
    let word = given_word.toUpperCase()
    let chars = _.shuffle(Array.from(word))
        return {
            word,
            chars,
            attempt : 1,
            guess : '',
            completed : false
        }
}
const refreshPage = () => window.location.reload(false); // สร้างฟังก์ชันเพื่อทำการ refresh browser เมื่อทำการเลือกคำถูก

const WordCard = (props) => { /* ประกาศ functional component ที่มีชื่อเป็น Wordcard */
const [gameState,setGameState] = useState({ /* ทำการสร้าง state เพื่อทำการเก็บค่าทั้งหมดของเกม */
    word:'',
    chars:'',
    attempt : 1,
    guess : '',
    completed : false
})

useEffect(()=>{ /* ทำการโหลดค่าต่าง ๆ ที่ถูกส่งมาจาก parent component แล้วเรียกใช้ lodash เพื่อทำการสลับตำแหน่งอักษร และกำหนด state เริ่มต้นให้กับ component*/
    let data = prepareStateFromWord(props.value)
    setGameState({
        ...gameState,
        word: data.word,
        chars: data.chars,
        attempt: data.attempt,
        guess: data.guess,
        completed: data.completed,
    })
},[])


const activationHandler = c => { /* ฟังก์ชันสำหรับรับค่าคำตอบจากผู้ใช้ */ 
    console.log(`${c} has been activated.`)
    
    let guess = gameState.guess + c /* นำค่าใน guess ที่เป็น String มาต่อกับ ค่า c ที่รับมาจากผู้ใช้ เพื่อนำอักษรแต่ละตัวที่ผู้ใช้เดามามาต่อเป็นคำ */ 
        setGameState({...gameState , guess})
        // console.log('chars length : ',gameState.guess)

        if (guess.length === gameState.word.length) {  /* ทำการตรวจสอบว่าคำตอบที่ผู้เล่นเดามามีความยาวเท่ากับ ความยาวของคำตอบหรือไม่ */ 
            if (guess === gameState.word) { /* ถ้าคำที่เดามาตรงกับ คำตอบให้แสดงคำตอบว่า ถูก  */ 
                console.log('Yeah')
                alert('😊 Correct 😊')
                setGameState({ ...gameState , guess: '', completed: true })
                refreshPage()
            } else {
                console.log('Reset')
                alert('😂 Wrong!! Try Again 😂') /* ถ้าคำที่เดามาไม่ตรงกับ คำตอบให้แสดงคำตอบว่า ผิด  */ 
                setGameState({ ...gameState , guess: '', attempt: gameState.attempt++ })
                refreshPage()
            }
        }
}
    return(
        <div>
            { Array.from(gameState.chars)
            .map(( c,i ) => {
               return (
                    <CharacterCard 
                        value = { c } 
                        key = { i }
                        activationHandler = {activationHandler}
                        attempt = {gameState.attempt}
                    />
               )
            }
                
            ) }
        </div>
    )
}

export default WordCard